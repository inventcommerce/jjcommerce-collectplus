<?php

/**
 * CollectPlus
 *
 * @category    CollectPlus
 * @package     Jjcommerce_CollectPlus
 * @author      Jjcommerce Team
 *
 */

namespace Jjcommerce\CollectPlus\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Customer\Api\AddressRepositoryInterface;

class AfterPlaceOrderRemoveCollectplusAddress implements ObserverInterface
{
    /**
     * Core store config
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     */

    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Sales\Model\Order $order,
        OrderRepositoryInterface $orderRepository,
        AddressRepositoryInterface $customerAddressRepository
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->order = $order;
        $this->orderRepository = $orderRepository;
        $this->customerAddressRepository = $customerAddressRepository;
    }


    /**
     * Delete saved collectplus address
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $order = $observer->getOrder();

        if($order && strpos($order->getShippingMethod(),"collect_collect") !== false) {
            $shippingAddressId = $order->getShippingAddressId();
            $shippingAddress = $order->getShippingAddress();
            if(is_object($shippingAddress) && $customerShippingAddressId = $shippingAddress->getCustomerAddressId()) {
                $customerShippingAddress = $this->customerAddressRepository->getById($customerShippingAddressId);
                $this->customerAddressRepository->delete($customerShippingAddress);
            }
        }
    }

    public function getConfigValue($path)
    {
        return $this->_scopeConfig->getValue(
            $path,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

}
