<?php

/**
 * CollectPlus
 *
 * @category    CollectPlus
 * @package     Jjcommerce_CollectPlus
 * @author      Jjcommerce Team
 *
 */

namespace Jjcommerce\CollectPlus\Plugin\Quote;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Model\Context;
use Magento\Quote\Model\Quote\Address as QuoteAddress;
use Magento\Shipping\Model\CarrierFactoryInterface;

class Address
{
    /**
     * @var CarrierFactoryInterface
     */
    protected $carrierFactory;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    public function __construct(
        Context $context,
        CarrierFactoryInterface $carrierFactory,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->carrierFactory = $carrierFactory;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Retrieve all grouped shipping rates, additionally sort collectplus shipping rates.
     *
     * @param QuoteAddress $subject
     * @param $result
     * @return array
     */
    public function afterGetGroupedAllShippingRates(QuoteAddress $subject, $result)
    {
        if (!$this->scopeConfig->getValue('carriers/collect/active')) {
            return $result;
        }
        $rates = [];
        $collectSortOrders = $this->scopeConfig->getValue('carriers/collect/sort_methods');
        $collectSortOrders = explode(',', $collectSortOrders);
        $_collectSortOrders = [];
        foreach ($collectSortOrders as $sortNo) {
            switch ($sortNo) {
                case '24':
                    $_collectSortOrders[] = 'collect_next';break;
                case '48':
                    $_collectSortOrders[] = 'collect_48hr';break;
                case '72':
                    $_collectSortOrders[] = 'collect_72hr';break;
                default: $_collectSortOrders = [];
            }
        }
        $_collectSortOrders = array_flip($_collectSortOrders);
        $_collectSortOrders = is_array($_collectSortOrders) ? $_collectSortOrders : [];
        $flag = false;
        foreach ($subject->getShippingRatesCollection() as $rate) {
            if (!$rate->isDeleted() && $this->carrierFactory->get($rate->getCarrier())) {
                if ($subject->getQuote()->getAgentData()) {
                    $pos = strpos($rate->getCode(), 'collect_');
                    $poserror = strpos($rate->getCode(), 'collect_error');
                    if ($pos === false) {
                        continue;
                    }
                    if (!isset($rates[$rate->getCarrier()])) {
                        $rates[$rate->getCarrier()] = [];
                    }
                    if (!empty($_collectSortOrders)) {
                        $posMothod = isset($_collectSortOrders[$rate->getMethod()]) ? $_collectSortOrders[$rate->getMethod()] : rand(100, 200);
                        $rates[$rate->getCarrier()][$posMothod] = $rate;
                    } else {
                        $rates[$rate->getCarrier()][] = $rate;
                    }
                    ksort($rates[$rate->getCarrier()]);
                    $flag = true;
                } else {
                    if (!isset($rates[$rate->getCarrier()])) {
                        $rates[$rate->getCarrier()] = [];
                    }

                    $rates[$rate->getCarrier()][] = $rate;
                    $rates[$rate->getCarrier()][0]->carrier_sort_order = $this->carrierFactory->get(
                        $rate->getCarrier()
                    )->getSortOrder();
                }
            }
        }
        uasort($rates, [$this, '_sortRates']);

        return $rates;
    }

    /**
     * Sort rates recursive callback
     *
     * @param array $firstItem
     * @param array $secondItem
     * @return int
     */
    protected function _sortRates($firstItem, $secondItem)
    {
        return (int) $firstItem[0]->carrier_sort_order <=> (int) $secondItem[0]->carrier_sort_order;
    }
}
